class Ckeditor::ApplicationController < ApplicationController
  layout 'ckeditor/application'

  before_filter :find_asset, :only => [:destroy]
  before_filter :ckeditor_authorize!
  before_filter :authorize_resource

  def client
    if session[:app_id].present?
      app = App.find(session[:app_id])
      client = app.client
    elsif session[:connected_to].present?
      client = Client.find(session[:connected_to])
    else
      raise "Client not found"
    end

    client
  end

  def users_scope
    #put the current users id into the array and bring back all their uploads
    #as well as the current clients
    if current_user.superuser?
      user_ids = client.agency.users.map(&:id) << current_user.id
    else
      user_ids = client.agency.users.map(&:id)
    end

    {
      assetable_id: user_ids
    }
  end

  protected

  def respond_with_asset(asset)
    file = params[:CKEditor].blank? ? params[:qqfile] : params[:upload]
    asset.data = Ckeditor::Http.normalize_param(file, request)

    callback = ckeditor_before_create_asset(asset)

    if callback && asset.save
      if params[:CKEditor].blank?
        render :json => asset.to_json(:only=>[:id, :type])
      else
        render :text => %Q"<script type='text/javascript'>
              window.parent.CKEDITOR.tools.callFunction(#{params[:CKEditorFuncNum]}, '#{config.relative_url_root}#{Ckeditor::Utils.escape_single_quotes(asset.url_content)}');
            </script>"
      end
    else
      if params[:CKEditor].blank?
        render :nothing => true, :format => :json
      else
        render :text => %Q"<script type='text/javascript'>
              window.parent.CKEDITOR.tools.callFunction(#{params[:CKEditorFuncNum]}, null, '#{Ckeditor::Utils.escape_single_quotes(asset.errors.full_messages.first)}');
            </script>"
      end
    end
  end
end
